import {colombia} from '../store/colombia'
import { useState, useEffect } from 'react';
import { useForm } from "react-hook-form";
import { DateTime } from "luxon";

export default function Form({submitForm, cita={}, ...props}) {
  const [cities, setCities] = useState([]);
  const [department, setDepartment] = useState([]);
  const [city, setCity] = useState('');
  const { register, handleSubmit, formState: { errors } } = useForm();
  const now = DateTime.now().setLocale("ar");

  useEffect(()=>{
    const departamento = cita?.id ? cita?.department : "Amazonas"
    setDepartment(departamento);
    makeCities(departamento)

  },[cita])

const makeCities = (departamento) =>{
  const new_cities = colombia.filter(c=>c.departamento==departamento)[0]["ciudades"] ?? [];
  setCities(new_cities);
  setCity(new_cities[0]);

}




  const onSubmit = data => {
    data.city = city;
    data.department = department;
    if(cita?.id){
      data.affair =  data.affair || cita.affair;
      data.answer =  data.answer || cita.answer;
      data.company =  data.company || cita.company;
    }
    submitForm(data)
  };
return(
  <form className="row g-3" onSubmit={handleSubmit(onSubmit)}>
      <div className="col-md-4">
        <label htmlFor="Fecha" className="form-label">Fecha</label>
        <input  type="date"
                className="form-control"
                id="Fecha"
                defaultValue={cita?.date ? cita?.date : now.toISODate()}
                min={now.toISODate()}
                {...register("date")}/>
      </div>
      <div className="col-md-4">
        <label htmlFor="hora_inicio" className="form-label">Inicio de Atención</label>
        <input  type="time"
                className="form-control"
                id="hora_inicio"
                defaultValue={cita?.init_time?cita?.init_time:now.toFormat('T')}
                {...register("init_time")}/>
      </div>
      <div className="col-md-4">
        <label htmlFor="hora_final" className="form-label">Fin de Atención</label>
        <input  type="time"
                className="form-control"
                id="hora_final"
                defaultValue={cita?.finish_time?cita?.finish_time:now.toFormat('T')}
                {...register("finish_time")}/>
      </div>
      <div className="col-12">
        <label htmlFor="empresa" className="form-label">Empresa</label>
        <input  type="text"
                className="form-control"
                id="empresa"
                defaultValue={cita?.company?cita?.company:""}
                placeholder={"Nombre de la Empresa"}
                {...register("company")} />
      </div>
      <div className="col-md-6">
        <label htmlFor="departamento" className="form-label">Departamento</label>
        <select id="departamento" className="form-select" onChange={(e)=>{setDepartment(e.target.value);makeCities(e.target.value)}}>
          {department&&<option value={department} selected>{department}</option>}
          {colombia.map(d=>(
              <option key={d.id} value={d.departamento} >{d.departamento}</option>
            ))}

        </select>
      </div>
      <div className="col-md-6">
        <label htmlFor="ciudad" className="form-label">Ciudad</label>
        <select id="ciudad" className="form-select" onChange={(e)=>setCity(e.target.value)}>
          {city&&<option value={city} selected>{city}</option>}
        {cities?.map(c=>(
            <option key={c} value={c}>{c}</option>
            )
        )}
        </select>
      </div>
      <div className="col-12">
        <label htmlFor="asunto" className="form-label">Asunto</label>
        <textarea className="form-control"
                  id="asunto"
                  defaultValue={cita?.affair?cita?.affair:""}
                  placeholder={"Asunto..."}
                  {...register("affair")}/>
      </div>
      <div className="col-12">
        <label htmlFor="respuesta" className="form-label">Respuesta</label>
        <textarea className="form-control"
                  id="respuesta"
                  defaultValue={cita?.answer?cita?.answer:""}
                  placeholder={"Respuesta..."}
                  {...register("answer")}/>
      </div>
      <div className="col-12">
      <div className="d-grid px-5">
      <button type="submit" className="btn btn-outline-primary ">
        {cita?.id?"Editar":"Guardar"}
      </button>
    </div>

      </div>
  </form>
)
}
