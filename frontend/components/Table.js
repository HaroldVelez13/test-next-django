import {DateTime} from "luxon";
import { useRouter } from 'next/router'



export default function Table({citas}) {
  const router = useRouter()

  const handleClick = (id)=>{
    router.push('/detalle/'+id)
  }
  return (

  <table className="table table-hover  mb-0">
    <thead>
      <tr className="table-info">
        <th scope="col">Fecha</th>
        <th scope="col">Hora Atención</th>
        <th scope="col">Hora Final</th>
        <th scope="col">Empresa</th>
        <th scope="col">Ciudad</th>
        <th scope="col">Asunto</th>
        <th scope="col">Respuesta</th>
        <th scope="col">Fecha Solicitud</th>
      </tr>
    </thead>
    <tbody>
    {citas.map(cita=>(
      <tr key={cita.id} onClick={()=>handleClick(cita.id)}>
        <td scope="row">{DateTime.fromSQL(cita.date).toFormat('DD')}</td>
        <td>{cita.init_time.slice(0, -3)}</td>
        <td>{cita.finish_time.slice(0, -3)}</td>
        <td>{cita.company}</td>
        <td>{cita.city}</td>
        <td>{cita.affair}</td>
        <td>{cita.answer}</td>
        <td>{DateTime.fromISO(cita.created_at).toFormat('ff')}</td>
      </tr>
    ))}
    </tbody>
  </table>
  )
}
