import NavBar from '../components/NavBar';
import Form from '../components/Form';
import { useRouter } from 'next/router';
import axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default function Create() {
  const router = useRouter();

  const handleClick = (e) => {
   e.preventDefault()
   router.push('/')
 }

 const notify = async() => {
   await toast("¡La cita fue creada con exito!");
   setTimeout(()=>router.push('/'),5300);
 };

 const handleSubmit = (data)=>{
   console.log("en crear", data)
   const headers = {"Content-Type" : "application/json"}
   axios.post('http://localhost:8000/appointment/api/', data, {headers:headers})
    .then(function (response) {
      const data = response.data
      notify();
    })
    .catch(function (error) {
      console.log(error);
    });
 }
  return (
    <>
    <NavBar />
    <div className="p-5 ">
      <div className="w-full card shadow-sm">
        <div className="card-header">
          Formulario de creación
        <button className="float-end btn btn-primary btn-sm"
                type="button"
                onClick={handleClick}
                title="Volver a la lista">
          <i className="bi bi-list" role="img" aria-label="List"></i>
        </button>
        </div>
        <div className="flex p-3">
        <Form submitForm={handleSubmit} cita={{}}/>
        </div>
      </div>
    </div>
    <ToastContainer/>
    </>
  )
}
