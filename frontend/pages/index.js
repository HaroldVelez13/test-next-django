import NavBar from '../components/NavBar';
import Table from '../components/Table';
import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';
import axios from "axios"

export default function Home() {
  const router = useRouter();
  const [citas, setCitas] = useState([]);

  useEffect(()=>{
    axios.get('http://localhost:8000/appointment/api/')
     .then(function (response) {
       const data = response.data
       setCitas(data)
     })
     .catch(function (error) {
       console.log(error);
     });
  },[])
  const handleClick = (e) => {
   e.preventDefault()
   router.push('/crear')
 }
  return (
    <>
    <NavBar />
    <div className="p-5 ">
      <div className="w-full card shadow-sm">
        <div className="card-header">
          Lista de citas
        <button className="float-end btn btn-primary btn-sm"
                type="button"
                onClick={handleClick}
                title="Crear">
          <i className="bi bi-plus-circle" role="img" aria-label="Add"></i>
        </button>
        </div>
        <div className="flex">
          <Table citas={citas}/>
        </div>
      </div>
    </div>
    </>
  )
}
