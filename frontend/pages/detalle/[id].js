import NavBar from '../../components/NavBar';
import Form from '../../components/Form';
import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';
import axios from "axios";
import {DateTime} from "luxon";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


export default function Detalle() {
  const router = useRouter();
  const [cita, setCita] = useState([]);
  const [id, setId] = useState([]);
  const [del, setDelete] = useState(false);

  useEffect(()=>{
    const _id = router.query.id
    setId(_id)
    axios.get('http://localhost:8000/appointment/api/'+_id+'/')
     .then(function (response) {
       const data = response.data ?? {}
       setCita(data)
     })
     .catch(function (error) {
       console.log(error);
     });
  },[])
  const handleClick = (e) => {
   e.preventDefault()
   router.push('/')
 }
 const handleClickEdit = (e) => {
  e.preventDefault()
  router.push('/editar/'+id)
}

const notify = async() => {
  await toast("¡La cita #00"+id+" fue Eliminada con exito!");
  setTimeout(()=>router.push('/'),5300);
};

const handleClickDelete = (e)=>{
  e.preventDefault();
  const headers = {"Content-Type" : "application/json"}
  axios.delete('http://localhost:8000/appointment/api/'+id+'/', {headers:headers})
   .then(function (response) {
     const data = response.data
     notify();
   })
   .catch(function (error) {
     console.log(error);
   });
}

  return (
    <>
    <NavBar />
    <div className="p-5 ">
      <div className="w-full card shadow-sm">
        <div className="card-header">
          Detalle de la cita #000{cita?.id}
          {del&&(
            <span className="bg-success">
            <button className="float-end btn btn-danger btn-sm mx-1"
                    type="button"
                    onClick={handleClickDelete}
                    title="Eliminar">
                    <i>Si</i>
            </button>
            <button className="float-end btn btn-primary btn-sm mx-1"
                    type="button"
                    onClick={()=>setDelete(false)}
                    title="Eliminar">
                    <i>No</i>
            </button>
          </span>
          )}
          <button className="float-end btn btn-danger btn-sm mx-1"
                  type="button"
                  onClick={()=>setDelete(true)}
                  title="Eliminar">
            <i className="bi bi-trash" role="img" aria-label="Eliminar"></i>
          </button>
        <button className="float-end btn btn-warning btn-sm mx-1"
                type="button"
                onClick={handleClickEdit}
                title="Editar">
          <i className="bi bi-pencil-square" role="img" aria-label="Edit"></i>
        </button>
        <button className="float-end btn btn-primary btn-sm mx-1"
                type="button"
                onClick={handleClick}
                title="Volver a la lista">
          <i className="bi bi-list" role="img" aria-label="List"></i>
        </button>
        </div>
        <div className="flex p-3">
        <div className=" mb-3">
          <div className="card-body">
            <h5 className="card-title mb-3">{cita?.company}</h5>
            <p className="card-text mb-2">Fecha:   <small className="text-muted">{DateTime.fromISO(cita?.date).toFormat('FF')}</small></p>
            <p className="card-text mb-2">Hora Atención:   <small className="text-muted">{cita?.init_time?.slice(0, -3)}</small></p>
            <p className="card-text mb-2">Hora Final:   <small className="text-muted">{cita?.finish_time?.slice(0, -3)}</small></p>
            <p className="card-text mb-2">Departamento:   <small className="text-muted">{cita?.department}</small></p>
            <p className="card-text mb-2">Ciudad:   <small className="text-muted">{cita?.city}</small></p>
            <p className="card-text mb-2">Asunto:   <small className="text-muted">{cita?.affair ?cita?.affair: " Sin Asunto "}</small></p>
            <p className="card-text mb-2">Respuesta:   <small className="text-muted">{cita?.answer?cita?.answer: " Sin Respuesta "}</small></p>
            <p className="card-text mt-3 mb-0">
              <small className="text-muted">Creada: {DateTime.fromISO(cita?.created_at).toFormat('ff')}</small>
              <br/>
              <small className="text-muted">Editada: {DateTime.fromISO(cita?.update_at).toFormat('ff')}</small>
            </p>
          </div>
        </div>
        </div>
      </div>
    </div>
    <ToastContainer/>
    </>
  )
}
