import NavBar from '../../components/NavBar';
import Form from '../../components/Form';
import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';
import axios from "axios";
import {DateTime} from "luxon";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default function Detalle() {
  const router = useRouter();
  const [cita, setCita] = useState([]);
  const [id, setId] = useState(0);

  useEffect(()=>{
    const _id = router.query.id
    setId(_id)
    axios.get('http://localhost:8000/appointment/api/'+_id+'/')
     .then(function (response) {
       const data = response.data ?? {}
       setCita(data)
     })
     .catch(function (error) {
       console.log(error);
     });
  },[])
  const handleClick = (e) => {
   e.preventDefault()
   router.push('/')
 }
 const notify = async() => {
   await toast("¡La cita #00"+id+" fue Editada con exito!");
   setTimeout(()=>router.push('/'),5300);
 };

 const handleSubmit = (data)=>{
   console.log("en crear", data)
   const headers = {"Content-Type" : "application/json"}
   axios.put('http://localhost:8000/appointment/api/'+id+'/', data, {headers:headers})
    .then(function (response) {
      const data = response.data
      notify();
    })
    .catch(function (error) {
      console.log(error);
    });
 }

  return (
    <>
    <NavBar />
    <div className="p-5 ">
      <div className="w-full card shadow-sm">
        <div className="card-header">
          Formulario de edición para la cita #000{id}
        <button className="float-end btn btn-primary btn-sm"
                type="button"
                onClick={handleClick}
                title="Volver a la lista">
          <i className="bi bi-list" role="img" aria-label="List"></i>
        </button>
        </div>
        <div className="flex p-3">

          <Form submitForm={handleSubmit} cita={cita} />
        </div>
      </div>
    </div>
    <ToastContainer/>
    </>
  )
}
