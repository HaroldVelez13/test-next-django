from django.urls import include, path
from .api import AppointmentApi
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'api', AppointmentApi)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
]
