from rest_framework import viewsets
from rest_framework.response import Response
from .serializer import AppointmentSerializer
from .models import Appointment



class AppointmentApi(viewsets.ModelViewSet):
    queryset = Appointment.objects.all().order_by('created_at')
    serializer_class = AppointmentSerializer
