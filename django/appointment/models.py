from django.db import models
from django.utils import timezone
from django.core.exceptions import ValidationError


# Create your models here.
class Appointment(models.Model):
    date = models.DateField()
    init_time = models.TimeField()
    finish_time = models.TimeField()
    company = models.CharField(max_length=150)
    department = models.CharField(max_length=150)
    city = models.CharField(max_length=150)
    affair = models.TextField(null=True, blank=True)
    answer = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        #on Create
        if not self.id and self.date<timezone.now().date():
            raise ValidationError(
                ('%(value)s cannot be later than today'),
                params={'value': self.date},
            )
        return super(Appointment, self).save(*args, **kwargs)
