#!/bin/sh

sleep 0.1
echo "Init makemigrations"
python manage.py makemigrations

sleep 0.1
echo "Init migrate"
python manage.py migrate

exec "$@"
