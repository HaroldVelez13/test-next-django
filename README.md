# Django - Next - Docker
## _Proyect Api rest with django and forntend with Nextjs_

### Install all with docker
- in the root folder excecute
docker-compose up --build
- Open api:
localhost:8000/appointment/api
- Open interface in:
localhost:3000/

### Install only Django
- Install Django dependencies in the folder django
pip install -r requirements.txt
- Set env from database
DB_PORT, DB_HOST , DB_NAME, DB_USER, DB_PASS
- Init makemigrations
python manage.py makemigrations
- Init migrate
python manage.py migrate
- run server
python manage.py runserver  0.0.0.0:8000
- Open api:
localhost:8000/appointment/api

### Install only React
- Install Django dependencies in the folder frontend
npm i
- Run server
npm run dev
- Open interface in:
localhost:3000/
